package com.zuitt.example;

import java.util.Scanner;

public class Exercise {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        System.out.println("Please enter a year:");
        int userAnswer = userInput.nextInt();

        if (userAnswer % 4 == 0 && userAnswer % 100 >= 1) {
            System.out.println("Year " + userAnswer + " is a leap year.");
        } else if (userAnswer % 400 == 0) {
            System.out.println("Year " + userAnswer + " is a leap year.");
        } else {
            System.out.println("Year " + userAnswer + " is not a leap year.");
        }
    }
}
