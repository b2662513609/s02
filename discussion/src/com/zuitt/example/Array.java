package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    public static void main(String[] args) {
        //[SECTION] Java Collection
        // Are single unit of objects
        // Data with relevant and connected values
        //[SECTION] Arrays
        // In Java, arrays are container of values of the same type given a predefined amount of values
        // Java arrays are more rigid, once the size and the data type are defined, they can no longer be changed
        // Syntax: Array Declaration
        // dataType[] identifier = new dataType[numOfElements];

        int[] intArray = new int[5];

        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;

        System.out.println(intArray);
        System.out.println(Arrays.toString(intArray));

        String[] names = {"John", "Jane", "Joe"};
        System.out.println(Arrays.toString(names));

        Arrays.sort(intArray);
        System.out.println("Order of items after sort() method: " + Arrays.toString(intArray));

        String[][] classroom = new String[3][3];

        //first row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Portos";
        classroom[0][2] = "Aramis";
        //second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";
        //third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.deepToString(classroom));

        //[SECTION] ArrayList
        // are resizeable arrays, wherein elements can be added or removed
        //Syntax: ArrayList
        //ArrayList<T> identifier = new ArrayList<T>();

        //Declaring ArrayList
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane", "Mike"));

        //Add elements
        students.add("John");
        students.add("Paul");
        students.add("Rafhael");

        System.out.println(students);

        System.out.println(students.get(0));

        students.add(0, "Joey");
        System.out.println(students);

        students.set(0, "George");
        System.out.println(students);

        students.remove(1);
        System.out.println(students);

        students.clear();
        System.out.println(students);

        System.out.println(students.size());

        //[SECTION] Hashmaps
        //Syntax: HashMap
        //HashMap<T, T> identifier = new HashMap<T, T>();

        HashMap<String, Integer> job_position = new HashMap<String, Integer>();

        //adding element to a HashMap
        job_position.put("Brandon", 5);
        job_position.put("Alice", 3);
        System.out.println(job_position);
        System.out.println(job_position.get("Brandon"));
        System.out.println(job_position.values());
        job_position.clear();
        System.out.println(job_position);
    }
}
